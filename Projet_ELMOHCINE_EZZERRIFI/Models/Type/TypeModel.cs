﻿using Projet_ELMOHCINE_EZZERRIFI.Models.Expanse;
using Projet_ELMOHCINE_EZZERRIFI.Models.Tva;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projet_ELMOHCINE_EZZERRIFI.Models.Type
{
    public class TypeModel
    {



   

        public Guid ExpenseType_ID { get; set; }
        public string Name { get; set; }
        public bool Fixed { get; set; }
        public bool OnlyManagers { get; set; }
        public Guid Tva_ID { get; set; }

        public virtual TvaModel Tvas { get; set; }

        public virtual List<ExpanseModel> Expanses { get; set; }

        public TypeModel()
        {
            this.Expanses = new List<ExpanseModel>();
        }


        public TypeModel(ExpanseTypes entite)
        {
            this.ExpenseType_ID = entite.ExpenseType_ID;
            this.Name = entite.Name;
            this.Fixed = entite.Fixed;
            this.OnlyManagers = entite.OnlyManagers;
            this.Tva_ID = entite.Tva_ID;

        }


    }
}