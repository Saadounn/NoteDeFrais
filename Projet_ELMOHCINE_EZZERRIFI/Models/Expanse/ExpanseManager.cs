﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projet_ELMOHCINE_EZZERRIFI.Models.Expanse
{
    public class ExpanseManager
    {

        public static List<ExpanseModel> map(List<Expanses> Expanses)
        {
            List<ExpanseModel> list = new List<ExpanseModel>();
            foreach (var item in Expanses) list.Add(new ExpanseModel(item));
            return list;
        }
        public static List<Expanses> map(List<ExpanseModel> Expanses)
        {
            List<Expanses> list = new List<Expanses>();
            foreach (var item in Expanses) list.Add(item.ToEntity());
            return list;
        }
    }
}