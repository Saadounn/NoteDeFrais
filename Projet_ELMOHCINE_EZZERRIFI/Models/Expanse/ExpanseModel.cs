﻿using Projet_ELMOHCINE_EZZERRIFI.Models.Client;
using Projet_ELMOHCINE_EZZERRIFI.Models.Projet;
using Projet_ELMOHCINE_EZZERRIFI.Models.Report;
using Projet_ELMOHCINE_EZZERRIFI.Models.Type;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projet_ELMOHCINE_EZZERRIFI.Models.Expanse
{
    public class ExpanseModel
    {
        public Guid Expanse_ID { get; set; }
        public Guid ExpanseReport_ID { get; set; }
        public int Day { get; set; }
        public Guid ExpanseType_ID { get; set; }
        public Guid Customer_ID { get; set; }
        public Guid Project_ID { get; set; }
        public double Amount_HT { get; set; }
        public double Amount_TVA { get; set; }
        public double Amount_TTC { get; set; }

        public virtual ClientModel Customers { get; set; }
        public virtual ReportModel ExpanseReports { get; set; }
        public virtual TypeModel ExpanseTypes { get; set; }
        public virtual ProjetModel Projects { get; set; }

        public ExpanseModel() { }
        public ExpanseModel(Expanses entite)
        {
            this.Expanse_ID = entite.Expanse_ID;
            this.ExpanseReport_ID = entite.ExpanseReport_ID;
            this.Day = entite.Day;
            this.ExpanseType_ID = entite.ExpanseType_ID;
            this.Customer_ID = entite.Customer_ID;
            this.Project_ID = entite.Project_ID;
            this.Amount_HT = entite.Amount_HT;
            this.Amount_TVA = entite.Amount_TVA;
            this.Amount_TTC = entite.Amount_TTC;

            this.Customers = new ClientModel(entite.Customers) ;
            this.ExpanseReports = new ReportModel(entite.ExpanseReports);
            this.ExpanseTypes = new TypeModel(entite.ExpanseTypes);
            this.Projects = new ProjetModel(entite.Projects);
        }


        public Expanses ToEntity()
        {
            Expanses entite = new Expanses();

            entite.Expanse_ID = this.Expanse_ID;
            entite.ExpanseReport_ID = this.ExpanseReport_ID;
            entite.Day = this.Day;
            entite.ExpanseType_ID = this.ExpanseType_ID;
            entite.Customer_ID = this.Customer_ID;
            entite.Project_ID = this.Project_ID;
            entite.Amount_HT = this.Amount_HT;
            entite.Amount_TVA = this.Amount_TVA;
            entite.Amount_TTC = this.Amount_TTC;


            return entite;
        }
    }
}