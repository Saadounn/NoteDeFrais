﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projet_ELMOHCINE_EZZERRIFI.Models.Tva
{
    public class TvaRepository
    {
        private NotesDeFraisEntities2 database { get; set; }

        public TvaRepository( NotesDeFraisEntities2 db)
        {
            this.database = db;
        }

        public List<Tvas> GetAll()
        {
            return this.database.Tvas.ToList();
        }

        public Tvas GetByID(Guid id)
        {
            return this.database.Tvas.Where(g => g.TVA_ID == id).FirstOrDefault();
        }

        public void Add(Tvas tva)
        {
            this.database.Tvas.Add(tva);
            this.database.SaveChanges();
        }

        public void Delete(Guid id)
        {
            this.database.Tvas.Remove(this.database.Tvas.Where(g => g.TVA_ID == id).FirstOrDefault());
            this.database.SaveChanges();
        }

    }
}