﻿using Projet_ELMOHCINE_EZZERRIFI.Models.Type;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Projet_ELMOHCINE_EZZERRIFI.Models.Tva
{
    public class TvaModel
    {

        public Guid TVA_ID { get; set; }
        [Required][DisplayName("Nom : ")]
        public string Name { get; set; }
        [Required][DisplayName("Valeur : ")]
        public double Value { get; set; }

        public virtual List<TypeModel> ExpanseTypes { get; set; }

        public TvaModel()
        {
            this.ExpanseTypes = new List<TypeModel>();
        }

        public TvaModel(Tvas entite)
        {
            this.TVA_ID = entite.TVA_ID;
            this.Name = entite.Name;
            this.Value = entite.Value;
        }

    }
}