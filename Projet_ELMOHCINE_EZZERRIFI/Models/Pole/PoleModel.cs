﻿using Projet_ELMOHCINE_EZZERRIFI.Models.Employee;
using Projet_ELMOHCINE_EZZERRIFI.Models.Projet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Projet_ELMOHCINE_EZZERRIFI.Models.Pole
{
    public class PoleModel
    {
       

        public Guid Pole_ID { get; set; }
        [DisplayName("Nom pôle : ")][Required(ErrorMessage = "Champ obligatoire !")]
        public string Name { get; set; }
        [DisplayName("Manageur du pôle : ")][Required(ErrorMessage = "Champ obligatoire !")]
        public Guid Manager_ID { get; set; }

        [DisplayName("Manageur du pôle : ")]
        public EmployeeModel Manager { get; set; }


        public PoleModel()
        {

        }


        public PoleModel(Poles pole)
        {
            this.Pole_ID = pole.Pole_ID;
            this.Name = pole.Name;
            this.Manager_ID = pole.Manager_ID;
        }

    }
}