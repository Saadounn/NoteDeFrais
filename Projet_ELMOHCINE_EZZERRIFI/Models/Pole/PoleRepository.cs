﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projet_ELMOHCINE_EZZERRIFI.Models.Pole
{
    public class PoleRepository
    {
        private NotesDeFraisEntities2 database { get; set; }

        public PoleRepository(NotesDeFraisEntities2 db) { this.database = db; }

        public List<Poles> GetAll()
        {
            return this.database.Poles.ToList();
        }

        public Poles GetById(Guid? id)
        {
            return this.database.Poles.Where(g => g.Pole_ID == id).FirstOrDefault();
        }

        public void Add(Poles pole)
        {
            this.database.Poles.Add(pole);
            this.database.SaveChanges();
        }

        public void Edit(Poles newPole)
        {
            Poles oldPole = GetById(newPole.Pole_ID);

            oldPole.Name = newPole.Name;
            oldPole.Manager_ID = newPole.Manager_ID;

            this.database.SaveChanges();

        }
    }
}