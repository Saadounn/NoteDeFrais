﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projet_ELMOHCINE_EZZERRIFI.Models.Employee
{
    public class EmployeeManager
    {


        public static List<EmployeeModel> map(List<Employees> empList)
        {
            List<EmployeeModel> modelList = new List<EmployeeModel>();

            foreach (var item in empList) modelList.Add(new EmployeeModel(item));

            return modelList;
        }

        /*
        public static List<Employees> map(List<EmployeeModel> model)
        {
            List<Employees> entite = new List<Employees>();

            foreach (var item in model) entite.Add(item.ToEntity());

            return entite;
        }
        //*/


    }
}