﻿using Projet_ELMOHCINE_EZZERRIFI.Models.Pole;
using Projet_ELMOHCINE_EZZERRIFI.Models.Report;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Projet_ELMOHCINE_EZZERRIFI.Models.Employee
{
    public class EmployeeModel
    {
        public Guid Employee_ID { get; set; }
        [DisplayName("Prénom :")][Required(ErrorMessage = "Champ obligatoire !")]
        public string FirstName { get; set; }
        [DisplayName("Nom : ")][Required(ErrorMessage = "Champ obligatoire !")]
        public string LastName { get; set; }
        [DataType(DataType.EmailAddress)]
        [DisplayName("Email : ")][Required(ErrorMessage = "Champ obligatoire !")]
        public string Email { get; set; }
        [DisplayName("Téléphone : ")]
        public string Telephone { get; set; }

        [DisplayName("Id pôle : ")]
        public Guid Pole_ID { get; set; }
        [DisplayName("Pôle : ")]
        public PoleModel Pole { get; set; }

        [DisplayName("Id rôle : ")]
        public Guid Role_ID { get; set; }
        [DisplayName("Rôle : ")]
        public RoleModel Role { get; set; }

        public EmployeeModel() {        }


        public EmployeeModel(Employees emp)
        {
            this.Employee_ID = emp.Employee_ID;
            this.FirstName = emp.FirstName;
            this.LastName = emp.LastName;
            this.Email = emp.Email;
            this.Telephone = emp.Telephone;
            
            if(emp.Poles != null)
            {
                this.Pole = new PoleModel(emp.Poles);
                this.Pole_ID = emp.Poles.Pole_ID;
            }
            if (emp.AspNetUsers.AspNetRoles.FirstOrDefault() != null)
            {
                this.Role = new RoleModel(emp.AspNetUsers.AspNetRoles.FirstOrDefault());
                this.Role_ID = Guid.Parse(emp.AspNetUsers.AspNetRoles.FirstOrDefault().Id);
            }

        }

        /*
        public Employees ToEntity()
        {

            Employees emp = new Employees();

            emp.Employee_ID = this.Employee_ID;
            emp.FirstName = this.FirstName;
            emp.LastName = this.LastName;
            emp.Email = this.Email;
            emp.Telephone = this.Telephone;
            
            return emp;
        }
        //*/
    }
}