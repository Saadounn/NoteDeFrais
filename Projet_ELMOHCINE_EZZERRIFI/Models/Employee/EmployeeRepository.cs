﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projet_ELMOHCINE_EZZERRIFI.Models.Employee
{
    public class EmployeeRepository
    {
        NotesDeFraisEntities2 database { get; set; }

        public EmployeeRepository(NotesDeFraisEntities2 db)
        {
            this.database = db;
        }


        public List<Employees> GetAll()
        {
            return this.database.Employees.Include("Poles").Include("AspNetUsers.AspNetRoles").ToList();
        }
        
        public void Add(EmployeeModel empMdl)
        {
            Employees emp = new Employees();
            AspNetUsers use = new AspNetUsers();

            use.Id = Guid.NewGuid().ToString();
            use.Email = empMdl.Email;
            use.EmailConfirmed = false;
            use.PasswordHash = "ALyk5vyaE1xL7k4oA6MyQMekbRdFdKF6iftzXrgM6CZlgrvDoI0/6IAw3uTnDoWhPQ==";
            use.SecurityStamp = null;
            use.PhoneNumber = null;
            use.PhoneNumberConfirmed = false;
            use.TwoFactorEnabled = false;
            use.LockoutEndDateUtc = null;
            use.LockoutEnabled = true;
            use.AccessFailedCount = 0;
            use.UserName = empMdl.Email;
            use.AspNetRoles.Add(this.database.AspNetRoles.Where(g => g.Id == empMdl.Role_ID.ToString()).FirstOrDefault());
            //use.AspNetRoles.Add(new AspNetRoles(Guid.NewGuid(), "Nouvel employé"));

            this.database.AspNetUsers.Add(use);
            this.database.SaveChanges();

            emp.Employee_ID = Guid.NewGuid();
            emp.FirstName = empMdl.FirstName;
            emp.LastName = empMdl.LastName;
            emp.Email = empMdl.Email;
            emp.Telephone = empMdl.Telephone;
            emp.User_ID = use.Id;
            emp.Pole_ID = empMdl.Pole_ID;


            this.database.Employees.Add(emp);
            this.database.SaveChanges();

        }
        
        public void Delete(Guid id)
        {
            Employees emp = this.database.Employees.Where(g => g.Employee_ID == id).FirstOrDefault();
            AspNetUsers user = this.database.AspNetUsers.Where(g => g.Id == emp.AspNetUsers.Id).FirstOrDefault();

            this.database.Employees.Remove(emp);
            this.database.AspNetUsers.Remove(user);
            this.database.SaveChanges();
        }

        public Employees GetById(Guid id)
        {
            return this.database.Employees.Where(g => g.Employee_ID == id).FirstOrDefault();
        }



    }
}