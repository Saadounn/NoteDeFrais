﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projet_ELMOHCINE_EZZERRIFI.Models.User
{
    public class UserRepository
    {
        public NotesDeFraisEntities2 database { get; set; }

        public UserRepository(NotesDeFraisEntities2 db) { this.database = db; }

        public void ChangeRole(AspNetUsers user,AspNetRoles role)
        {

            AspNetUsers oldUser = this.database.AspNetUsers.Where(g => g.Id == user.Id).FirstOrDefault();
            oldUser.AspNetRoles.Clear();
            oldUser.AspNetRoles.Add(role);

            this.database.SaveChanges();
        }

    }
}