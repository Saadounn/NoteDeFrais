﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projet_ELMOHCINE_EZZERRIFI.Models.Client
{
    public class ClientManager
    {

        public static List<ClientModel> map(List<Customers> clients)
        {
            List<ClientModel> list = new List<ClientModel>();
            foreach (var item in clients) list.Add(new ClientModel(item));
            return list;
        }

    }
}