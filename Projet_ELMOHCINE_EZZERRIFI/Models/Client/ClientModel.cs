﻿using Projet_ELMOHCINE_EZZERRIFI.Models.Expanse;
using Projet_ELMOHCINE_EZZERRIFI.Models.Projet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Projet_ELMOHCINE_EZZERRIFI.Models.Client
{
    public class ClientModel
    {

        [DisplayName("Id client : ")]
        public Guid Customer_ID { get; set; }
        [DisplayName("Nom, Prénom :")][Required(ErrorMessage = "Champ obligatoire !")]
        public string Name { get; set; }
        [DisplayName("Code n° :")][Required(ErrorMessage = "Champ obligatoire !")][StringLength(10)]
        public string Code { get; set; }

        [DisplayName("Projet(s) : ")]
        public IEnumerable<ProjetModel> Projets { get; set; }
        [DisplayName("Expanse(s)")]
        public IEnumerable<ExpanseModel> Expanses { get; set; }


        public ClientModel()
        {
            this.Projets = new List<ProjetModel>();
            this.Expanses = new List<ExpanseModel>();
        }


        public ClientModel(Customers entite)
        {
            this.Customer_ID = entite.Customer_ID;
            this.Name = entite.Name;
            this.Code = entite.Code;

            if(entite.Expanses.Count() !=0 ) this.Expanses = from g in entite.Expanses select new ExpanseModel(g);
            if(!entite.Projects.Any()) this.Projets = from g in entite.Projects select new ProjetModel(g);
        }

    }
} 