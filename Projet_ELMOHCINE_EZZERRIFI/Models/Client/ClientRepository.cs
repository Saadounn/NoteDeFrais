﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projet_ELMOHCINE_EZZERRIFI.Models.Client
{
    public class ClientRepository
    {
        private NotesDeFraisEntities2 database { get; set; }

        public ClientRepository(NotesDeFraisEntities2 db)
        {
            this.database = db;
        }

        public List<Customers> GetAll()
        {
            return this.database.Customers.Include("Projects").Include("Expanses").ToList();
        }

        public void Delete(Guid id)
        {
            Customers client = this.database.Customers.Where(g => g.Customer_ID == id).FirstOrDefault();
            this.database.Customers.Remove(client);
            this.database.SaveChanges();
        }

        public void Add(Customers client)
        {
            this.database.Customers.Add(client);
            this.database.SaveChanges();
        }

        public Customers GetById(Guid id)
        {
            return this.database.Customers.Where(g => g.Customer_ID == id).FirstOrDefault();
        }

        public void Edit(Customers client)
        {
            Customers oldClient = this.database.Customers.Where(g => g.Customer_ID == client.Customer_ID).FirstOrDefault();

            oldClient.Name = client.Name;
            oldClient.Code = client.Code;

            this.database.SaveChanges();

        }

        public List<Projects> GetAllPojects(Guid id)
        {
            return this.database.Projects.Where(g => g.Customer_ID == id).ToList();
        }


    }
}