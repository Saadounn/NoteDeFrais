﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projet_ELMOHCINE_EZZERRIFI.Models.Report
{
    public class ReportManager
    {

        public static List<ReportModel> map(List<ExpanseReports> reports)
        {
            List<ReportModel> list = new List<ReportModel>();
            foreach (var item in reports) list.Add(new ReportModel(item));
            return list;
        }
        public static List<ExpanseReports> map(List<ReportModel> model)
        {
            List<ExpanseReports> list = new List<ExpanseReports>();
            foreach (var item in model) list.Add(item.ToEntity());

            return list;
        }

    }
}