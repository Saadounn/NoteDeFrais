﻿using Projet_ELMOHCINE_EZZERRIFI.Models.Employee;
using Projet_ELMOHCINE_EZZERRIFI.Models.Expanse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projet_ELMOHCINE_EZZERRIFI.Models.Report
{
    public class ReportModel
    {


        public Guid ExpanseReport_ID { get; set; }
        public Guid Employee_ID { get; set; }
        public Guid Author_ID { get; set; }
        public DateTime CreationDate { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int StatusCode { get; set; }
        public double Total_HT { get; set; }
        public double Total_TVA { get; set; }
        public double Total_TTC { get; set; }
        public string ManagerComment { get; set; }
        public string AccountingComment { get; set; }

        public virtual EmployeeModel Employees { get; set; }
        public virtual EmployeeModel Employees1 { get; set; }

        public virtual List<ExpanseModel> Expanses { get; set; }


        public ReportModel()
        {
            this.Expanses = new List<ExpanseModel>();
        }
        public ReportModel(ExpanseReports entite)
        {
            this.ExpanseReport_ID = entite.ExpanseReport_ID;
            this.Employee_ID = entite.Employee_ID;
            this.Author_ID = entite.Author_ID;
            this.CreationDate = entite.CreationDate;
            this.Year = entite.Year;
            this.Month = entite.Month;
            this.StatusCode = entite.StatusCode;
            this.Total_HT = entite.Total_HT;
            this.Total_TVA = entite.Total_TVA;
            this.Total_TTC = entite.Total_TTC;
            this.ManagerComment = entite.ManagerComment;
            this.AccountingComment = entite.AccountingComment;

            this.Employees = new EmployeeModel(entite.Employees);
            this.Employees1 = new EmployeeModel(entite.Employees1);

            this.Expanses = ExpanseManager.map(entite.Expanses.ToList());

        }
        public ExpanseReports ToEntity()
        {
            ExpanseReports entite = new ExpanseReports();



            return entite;
        }
    }
}