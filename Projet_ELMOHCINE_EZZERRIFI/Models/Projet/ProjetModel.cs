﻿using Projet_ELMOHCINE_EZZERRIFI.Models.Client;
using Projet_ELMOHCINE_EZZERRIFI.Models.Expanse;
using Projet_ELMOHCINE_EZZERRIFI.Models.Pole;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Projet_ELMOHCINE_EZZERRIFI.Models.Projet
{
    public class ProjetModel
    {

        public Guid Project_ID { get; set; }
        [DisplayName("Nom : ")][Required(ErrorMessage = "Champ obligatoire !")]
        public string Name { get; set; }
        [DisplayName("Desctiption : ")]
        public string Description { get; set; }
        [DisplayName("Budget : ")]
        public double Budget { get; set; }
        [DisplayName("Client : ")][Required(ErrorMessage = "Champ obligatoire !")]
        public Guid Customer_ID { get; set; }
        [DisplayName("Pôle : ")][Required(ErrorMessage = "Champ obligatoire !")]
        public Guid Pole_ID { get; set; }

        public virtual ClientModel Client { get; set; }
        public virtual PoleModel Poles { get; set; }

        [DisplayName("Expanse(s)")]
        public virtual IEnumerable<ExpanseModel> Expanses { get; set; }

        public ProjetModel()
        {
            this.Expanses = new List<ExpanseModel>();
        }

        public ProjetModel(Projects entite)
        {
            this.Project_ID = entite.Project_ID;
            this.Name = entite.Name;
            this.Description = entite.Description;
            this.Budget = entite.Budget;
            this.Customer_ID = entite.Customer_ID;
            this.Pole_ID = entite.Pole_ID;

            if (entite.Customers != null) this.Client = new ClientModel(entite.Customers);
            //if (entite.Poles != null) this.Poles = new PoleModel(entite.Poles);
            //if (entite.Expanses != null) this.Expanses = from g in entite.Expanses select new ExpanseModel(g);

        }
        
    }
}