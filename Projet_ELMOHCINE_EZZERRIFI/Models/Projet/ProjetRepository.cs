﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projet_ELMOHCINE_EZZERRIFI.Models.Projet
{
    public class ProjetRepository
    {
        private NotesDeFraisEntities2 database { get; set; }

        public ProjetRepository(NotesDeFraisEntities2 db)
        {
            this.database = db;
        }
       
        public List<Projects> GetAll()
        {
            return this.database.Projects.Include("Customers").Include("Expanses").Include("Poles").ToList();
        }

        public void Add(Projects projet)
        {
            this.database.Projects.Add(projet);
            this.database.SaveChanges();
        }

        public Projects GetById(Guid id)
        {
            return this.database.Projects.Where(g => g.Project_ID == id).FirstOrDefault();
        }

        public void Delete(Projects proj)
        {
            this.database.Projects.Remove(proj);
            this.database.SaveChanges();
        }


        public List<Projects> GetByCustomer(Guid id)
        {
            return this.database.Projects.Where(g => g.Customer_ID == id).ToList();
        }

    }
}