﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projet_ELMOHCINE_EZZERRIFI.Models.Role
{
    public class RoleManager
    {
        public static AspNetRoles map(RoleModel role)
        {
            AspNetRoles entite = new AspNetRoles();
            entite.Id = role.Role_ID;
            entite.Name = role.Name;
            return entite;
        }

        public static List<RoleModel> map(List<AspNetRoles> list)
        {
            List<RoleModel> modelList = new List<RoleModel>();

            foreach (var item in list) modelList.Add(new RoleModel(item));

            return modelList;
        }

        public static List<AspNetRoles> map(List<RoleModel> model)
        {
            List<AspNetRoles> modelList = new List<AspNetRoles>();


            return modelList;
        }
    }
}