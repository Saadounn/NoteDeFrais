﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Projet_ELMOHCINE_EZZERRIFI.Models
{
    public class RoleModel
    {


        public string Role_ID { get; set; }
        [DisplayName("Libelle : ")][Required(ErrorMessage = "Champ obligatoire !")]
        public string Name { get; set; }


        public RoleModel() {       }
        public RoleModel(AspNetRoles role)
        {
            this.Role_ID = role.Id;
            this.Name = role.Name;
        }


    }
}