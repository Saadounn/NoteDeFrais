﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projet_ELMOHCINE_EZZERRIFI.Models.Role
{
    public class RoleRepository
    {
        public NotesDeFraisEntities2 database { get; set; }

        public RoleRepository(NotesDeFraisEntities2 db)
        {
            this.database = db;
        }

        public List<AspNetRoles> GetAll()
        {
            return this.database.AspNetRoles.ToList();
        }

        public AspNetRoles GetById(String id)
        {
            return this.database.AspNetRoles.Where(g => g.Id == id).FirstOrDefault();
        }

        public void Add(AspNetRoles role)
        {
            this.database.AspNetRoles.Add(role);
            this.database.SaveChanges();
        }

        public void Delete(Guid id)
        {
            this.database.AspNetRoles.Remove(this.database.AspNetRoles.Where(g => g.Id == id.ToString()).FirstOrDefault());
            this.database.SaveChanges();
        }

        public void Edit(AspNetRoles newRole)
        {
            AspNetRoles role = this.database.AspNetRoles.Where(g => g.Id == newRole.Id).FirstOrDefault();
            role.Name = newRole.Name;
            this.database.SaveChanges();
        }

        public List<AspNetUsers> GetAllUsersById(Guid id)
        {
            return this.database.AspNetRoles.Include("AspNetUsers").Where(g => g.Id == id.ToString()).FirstOrDefault().AspNetUsers.ToList();
        }

    }
}