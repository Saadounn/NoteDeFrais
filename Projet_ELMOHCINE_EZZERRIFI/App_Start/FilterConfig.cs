﻿using System.Web;
using System.Web.Mvc;

namespace Projet_ELMOHCINE_EZZERRIFI
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
