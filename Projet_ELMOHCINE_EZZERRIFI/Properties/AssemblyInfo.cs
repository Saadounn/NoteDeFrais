﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Les informations générales relatives à un assembly dépendent de 
// l’ensemble d’attributs suivant. Changez les valeurs de ces attributs pour modifier les informations
// associées à un assembly.
[assembly: AssemblyTitle("Projet_ELMOHCINE_EZZERRIFI")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Projet_ELMOHCINE_EZZERRIFI")]
[assembly: AssemblyCopyright("Copyright ©  2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// L’affectation de la valeur false à ComVisible rend les types invisibles dans cet assembly
// aux composants COM. Si vous devez accéder à un type dans cet assembly à partir de
// COM, affectez la valeur true à l’attribut ComVisible sur ce type.
[assembly: ComVisible(false)]

// Le GUID suivant est pour l’ID de la typelib si ce projet est exposé à COM
[assembly: Guid("7c8a518f-77e2-4222-a7fa-68a7dd6efb13")]

// Les informations de version pour un assembly se composent des quatre valeurs suivantes :
//
//      Version principale
//      Version mineure
//      Numéro de build
//      Révision
//
// Vous pouvez spécifier toutes les valeurs ou vous pouvez définir par défaut les valeurs Révision et Numéro de version
// en utilisant ’*’, comme indiqué ci-dessous :
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
