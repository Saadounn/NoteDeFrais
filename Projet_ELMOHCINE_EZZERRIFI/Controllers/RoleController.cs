﻿using Projet_ELMOHCINE_EZZERRIFI.Models;
using Projet_ELMOHCINE_EZZERRIFI.Models.Role;
using Projet_ELMOHCINE_EZZERRIFI.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Projet_ELMOHCINE_EZZERRIFI.Controllers
{
    public class RoleController : Controller
    {
        public ActionResult List()
        {
            using(var dao = new NotesDeFraisEntities2())
            {
                RoleRepository RoleRepo = new RoleRepository(dao);

                IEnumerable<RoleModel> ListRoles = from g in RoleRepo.GetAll() select new RoleModel(g);

                return View("List",ListRoles);
            }

        }

        public ActionResult Create()
        {
            return View("Create");
        }

        [HttpPost]
        public ActionResult CreateAction(RoleModel newRole)
        {
            using (var dao = new NotesDeFraisEntities2())
            {
                RoleRepository RoleRepo = new RoleRepository(dao);
                AspNetRoles role = new AspNetRoles();

                role.Id = Guid.NewGuid().ToString();
                role.Name = newRole.Name;

                RoleRepo.Add(role);

            }

                return RedirectToAction("List");
        }

        public ActionResult Delete(Guid id)
        {
            using(var dao = new NotesDeFraisEntities2())
            {
                RoleRepository RoleRepo = new RoleRepository(dao);
                UserRepository UserRepo = new UserRepository(dao);

                List<AspNetUsers> WithoutRoles = RoleRepo.GetAllUsersById(id);

                AspNetRoles UslessRole = RoleRepo.GetById("9671c647-a473-422d-aa70-cb3256f0f9dc");

                foreach(var item in WithoutRoles)
                {
                    UserRepo.ChangeRole(item, UslessRole);
                }

                RoleRepo.Delete(id);
            }
            return RedirectToAction("List");
        }

        public ActionResult Edit(Guid id)
        {
            using (var dao = new NotesDeFraisEntities2())
            {
                RoleRepository RoleRepo = new RoleRepository(dao);
                return View("Edit", new RoleModel(RoleRepo.GetById(id.ToString())));
            }
        }

        public ActionResult EditAction (RoleModel editedRole)
        {
            using (var dao = new NotesDeFraisEntities2())
            {
                RoleRepository RoleRepo = new RoleRepository(dao);

                AspNetRoles role = new AspNetRoles();

                role.Id = editedRole.Role_ID;
                role.Name = editedRole.Name;

                RoleRepo.Edit(role);
            }
            return RedirectToAction("List");
        }

    }
}