﻿using Projet_ELMOHCINE_EZZERRIFI.Models;
using Projet_ELMOHCINE_EZZERRIFI.Models.Tva;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Projet_ELMOHCINE_EZZERRIFI.Controllers
{
    public class TvaController : Controller
    {
        public ActionResult List()
        {
            using(var dao = new NotesDeFraisEntities2())
            {
                TvaRepository TvaRepo = new TvaRepository(dao);

                IEnumerable<TvaModel> list = from g in TvaRepo.GetAll() select new TvaModel(g);

                return View("List", list);
            }
        }

        public ActionResult Create()
        {
            return View("Create");
        }

        public ActionResult Edit(Guid TVA_ID)
        {
            using (var dao = new NotesDeFraisEntities2())
            {
                TvaRepository repo = new TvaRepository(dao);
                return View("Edit", new TvaModel(repo.GetByID(TVA_ID)));

            }
        }

        [HttpPost]
        public ActionResult EditAction(TvaModel tva)
        {


            using (var dao = new NotesDeFraisEntities2())
            {
                TvaRepository repo = new TvaRepository(dao);


            }


            return RedirectToAction("List");
        }

        [HttpPost]
        public ActionResult CreateAction(TvaModel tva)
        {

            using (var dao = new NotesDeFraisEntities2())
            {
                TvaRepository repo = new TvaRepository(dao);

                Tvas entite = new Tvas();
                entite.TVA_ID = Guid.NewGuid();
                entite.Name = tva.Name;
                entite.Value = tva.Value;

                repo.Add(entite);
            }
            return RedirectToAction("List");
        }

        public ActionResult Details(Guid TVA_ID)
        {
            using(var dao = new NotesDeFraisEntities2())
            {
                TvaRepository repo = new TvaRepository(dao);

                TvaModel Tva = new TvaModel(repo.GetByID(TVA_ID));
                return View("Details",Tva);
            }
        }

        public ActionResult Delete(Guid TVA_ID)
        {
            using (var dao = new NotesDeFraisEntities2())
            {
                TvaRepository repo = new TvaRepository(dao);
                
                repo.Delete(TVA_ID);

            }
                return RedirectToAction("List");
        }
    }
}