﻿using Projet_ELMOHCINE_EZZERRIFI.Models;
using Projet_ELMOHCINE_EZZERRIFI.Models.Client;
using Projet_ELMOHCINE_EZZERRIFI.Models.Projet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Projet_ELMOHCINE_EZZERRIFI.Controllers
{
    public class ClientController : Controller
    {

        public ActionResult List()
        {
            using (var dao = new NotesDeFraisEntities2())
            {
                ClientRepository CustRepo = new ClientRepository(dao);
                ProjetRepository ProjRepo = new ProjetRepository(dao);

                IEnumerable<ClientModel> ListClients = from g in CustRepo.GetAll() select new ClientModel(g);


                List<ClientModel> list = new List<ClientModel>();

                foreach (var client in ListClients)
                {

                    client.Projets = from g in CustRepo.GetAllPojects(client.Customer_ID) select new ProjetModel(g);

                    list.Add(client);
                }


                return View("List", list);
            }
        }

        public ActionResult Delete(Guid id)
        {
            using (var dao = new NotesDeFraisEntities2())
            {
                ClientRepository CustRepo = new ClientRepository(dao);

                CustRepo.Delete(id);
            }

            return RedirectToAction("List");
        }

        public ActionResult Create()
        {
            using (var dao = new NotesDeFraisEntities2())
            {
                ProjetRepository ProjRepo = new ProjetRepository(dao);

                ViewData["projets"] = from g in ProjRepo.GetAll() select new ProjetModel(g);
            }
            return View("Create");
        }

        [HttpPost]
        public ActionResult createAction(ClientModel newClient)
        {

            Customers client = new Customers();

            client.Customer_ID = Guid.NewGuid();
            client.Name = newClient.Name;
            client.Code = newClient.Code;

            using (var dao = new NotesDeFraisEntities2())
            {
                ClientRepository CustRepo = new ClientRepository(dao);
                CustRepo.Add(client);
            }
            return RedirectToAction("List");
        }

        public ActionResult Edit(Guid Customer_ID)
        {

            using(var dao = new NotesDeFraisEntities2())
            {
                ClientRepository custRepo = new ClientRepository(dao);

                ClientModel client = new ClientModel(custRepo.GetById(Customer_ID));

                return View("Edit", client);
            }
        }

        public ActionResult EditAction(ClientModel customer)
        {
            using(var dao = new NotesDeFraisEntities2())
            {
                ClientRepository custRepo = new ClientRepository(dao);

                Customers client = new Customers();

                client.Customer_ID = customer.Customer_ID;
                client.Name = customer.Name;
                client.Code = customer.Code;

                custRepo.Edit(client);
            }
            return RedirectToAction("List");
        }

        public ActionResult Details(Guid Customer_ID)
        {
            using(var dao = new NotesDeFraisEntities2()){
                ClientRepository custRepo = new ClientRepository(dao);
                ProjetRepository projRepo = new ProjetRepository(dao);

                ClientModel theCust = new ClientModel(custRepo.GetById(Customer_ID));
                theCust.Projets = from g in projRepo.GetByCustomer(theCust.Customer_ID) select new ProjetModel(g);
                return View("Details", theCust);
            }


        }




        }
}
