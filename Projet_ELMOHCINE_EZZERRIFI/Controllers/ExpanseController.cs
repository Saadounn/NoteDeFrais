﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Projet_ELMOHCINE_EZZERRIFI.Controllers
{
    public class ExpanseController : Controller
    {
        // GET: Expanse
        public ActionResult Index()
        {
            return View();
        }

        // GET: Expanse/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Expanse/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Expanse/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Expanse/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Expanse/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Expanse/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Expanse/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
