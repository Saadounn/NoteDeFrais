﻿using Projet_ELMOHCINE_EZZERRIFI.Models;
using Projet_ELMOHCINE_EZZERRIFI.Models.Client;
using Projet_ELMOHCINE_EZZERRIFI.Models.Pole;
using Projet_ELMOHCINE_EZZERRIFI.Models.Projet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Projet_ELMOHCINE_EZZERRIFI.Controllers
{
    public class ProjetController : Controller
    {
        public ActionResult List()
        {

            using(var dao = new NotesDeFraisEntities2())
            {
                ProjetRepository ProjRepo = new ProjetRepository(dao);
                PoleRepository PoleRepo = new PoleRepository(dao);

                IEnumerable<ProjetModel> ListProj = from g in ProjRepo.GetAll() select new ProjetModel(g);
                ListProj = ListProj.ToList();

                foreach (var i in ListProj)
                {
                    i.Poles = new PoleModel(PoleRepo.GetById(i.Pole_ID));
                }


                return View("List",ListProj);
            }

        }
        public ActionResult Create()
        {
            using(var dao = new NotesDeFraisEntities2())
            {
                PoleRepository PoleRepo = new PoleRepository(dao);
                ClientRepository CustRepo = new ClientRepository(dao);

                ViewData["poles"] = from g in PoleRepo.GetAll() select new PoleModel(g);
                ViewData["clients"] = from g in CustRepo.GetAll() select new ClientModel(g);

            }
            return View("Create");
        }

        [HttpPost]
        public ActionResult CreateAction(ProjetModel projet)
        {
            using (var dao = new NotesDeFraisEntities2())
            {
                ProjetRepository ProjRepo = new ProjetRepository(dao);
                PoleRepository PoleRepo = new PoleRepository(dao);
                ClientRepository CustRepo = new ClientRepository(dao);

                Projects entite = new Projects();

                entite.Project_ID = Guid.NewGuid();
                entite.Name = projet.Name;
                entite.Description = projet.Description;
                entite.Budget = projet.Budget;
                entite.Customer_ID = projet.Customer_ID;
                entite.Pole_ID = projet.Pole_ID;

                entite.Customers = CustRepo.GetById(projet.Customer_ID);
                entite.Poles = PoleRepo.GetById(projet.Pole_ID);

                ProjRepo.Add(entite);

            }
            return RedirectToAction("List");
        }


        public ActionResult Delete(Guid id)
        {
            using(var dao = new NotesDeFraisEntities2())
            {
                ProjetRepository ProjRepo = new ProjetRepository(dao);

                ProjRepo.Delete(ProjRepo.GetById(id));

            }

            return RedirectToAction("List");
        }


    }
}