﻿using Projet_ELMOHCINE_EZZERRIFI.Models;
using Projet_ELMOHCINE_EZZERRIFI.Models.Employee;
using Projet_ELMOHCINE_EZZERRIFI.Models.Pole;
using Projet_ELMOHCINE_EZZERRIFI.Models.Role;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Projet_ELMOHCINE_EZZERRIFI.Controllers
{
    public class EmployeeController : Controller
    {
        [Authorize(Roles="Administrateur")]
        public ActionResult ListEmployee()
        {
            using(var dao = new NotesDeFraisEntities2())
            {
                EmployeeRepository repo = new EmployeeRepository(dao);

                IEnumerable<EmployeeModel> ListEmployee = from g in repo.GetAll() select new EmployeeModel(g);

                return View("ListEmployee", ListEmployee);
            }

        }

        [Authorize(Roles = "Administrateur")]
        public ActionResult Create()
        {
            using (var dao = new NotesDeFraisEntities2())
            {
                PoleRepository PoleRepo = new PoleRepository(dao);
                RoleRepository RoleRepo = new RoleRepository(dao);

                ViewData["poles"] = from g in PoleRepo.GetAll() select new PoleModel(g);
                ViewData["roles"] = from g in RoleRepo.GetAll() select new RoleModel(g);

            }

            return View("Create");
        }

        [HttpPost]
        [Authorize(Roles = "Administrateur")]
        public ActionResult CreateAction(EmployeeModel newEmp)
        {
            if (ModelState.IsValid)
            {
                using (var dao = new NotesDeFraisEntities2())
                {
                    EmployeeRepository EmpRepo = new EmployeeRepository(dao);

                    EmpRepo.Add(newEmp);

                }
                return RedirectToAction("ListEmployee");
            }
            else return View("Error");
        }

        [HttpGet]
        [Authorize(Roles = "Administrateur")]
        public ActionResult Delete(Guid id)
        {
            using(var dao = new NotesDeFraisEntities2())
            {
                EmployeeRepository EmpRepo = new EmployeeRepository(dao);
                EmpRepo.Delete(id);
            }

            return RedirectToAction("ListEmployee");
        }

        [HttpGet]
        [Authorize(Roles = "Administrateur")]
        public ActionResult Edit(Guid id)
        {
            using(var dao = new NotesDeFraisEntities2())
            {
                EmployeeRepository EmpRepo = new EmployeeRepository(dao);
                
                EmployeeModel Employe =  new EmployeeModel(EmpRepo.GetById(id));

                    PoleRepository PoleRepo = new PoleRepository(dao);
                    RoleRepository RoleRepo = new RoleRepository(dao);

                    ViewData["poles"] = from g in PoleRepo.GetAll() select new PoleModel(g);
                    ViewData["roles"] = from g in RoleRepo.GetAll() select new RoleModel(g);


                return View("Edit", Employe);
            }
        }

        [HttpPost]
        [Authorize(Roles = "Administrateur")]
        public ActionResult EditAction(EmployeeModel employe)
        {
            return RedirectToAction("ListEmployee");
        }
    }
}