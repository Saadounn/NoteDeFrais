﻿using Projet_ELMOHCINE_EZZERRIFI.Models;
using Projet_ELMOHCINE_EZZERRIFI.Models.Employee;
using Projet_ELMOHCINE_EZZERRIFI.Models.Pole;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Projet_ELMOHCINE_EZZERRIFI.Controllers
{
    public class PoleController : Controller
    {
        public ActionResult List()
        {
            using (var dao= new NotesDeFraisEntities2() )
            {
                PoleRepository PoleRepo = new PoleRepository(dao);
                EmployeeRepository EmpRepo = new EmployeeRepository(dao);

                IEnumerable<PoleModel> polesWithoutManager = from g in PoleRepo.GetAll() select new PoleModel(g);

                List<PoleModel> poles = new List<PoleModel>();
                foreach(var pole in polesWithoutManager)
                {
                    pole.Manager = new EmployeeModel(EmpRepo.GetById(pole.Manager_ID));
                    poles.Add(pole);
                }

                return View("List", poles);
            }
        }

        public ActionResult Create()
        {
            using (var dao = new NotesDeFraisEntities2())
            {
                EmployeeRepository EmpRepo = new EmployeeRepository(dao);


                IEnumerable<EmployeeModel> employees = from g in EmpRepo.GetAll() select new EmployeeModel(g);
                ViewData["Employee"] = employees.Where(g => g.Role.Name == "Manageur");

            }
            return View("Create");
        }

        public ActionResult CreateAction(PoleModel pole)
        {
            using(var dao = new NotesDeFraisEntities2())
            {
                PoleRepository PoleRepo = new PoleRepository(dao);


                Poles p = new Poles();
                p.Name = pole.Name;
                p.Manager_ID = pole.Manager_ID;


                PoleRepo.Add(p);
            }


            return RedirectToAction("List");
        }

        public ActionResult Edit(Guid id)
        {
            using (var dao = new NotesDeFraisEntities2())
            {
                EmployeeRepository EmpRepo = new EmployeeRepository(dao);
                PoleRepository PoleRepo = new PoleRepository(dao);

                IEnumerable<EmployeeModel> employees = from g in EmpRepo.GetAll() select new EmployeeModel(g);
                ViewData["Employee"] = employees.Where(g => g.Role.Name == "Manageur");

                return View("Edit", new PoleModel(PoleRepo.GetById(id)));
            }
        }

        public ActionResult EditAction(PoleModel Pole)
        {

            using(var dao = new NotesDeFraisEntities2())
            {
                PoleRepository PoleRepo = new PoleRepository(dao);

                Poles pole = new Poles();

                pole.Pole_ID = Pole.Pole_ID;
                pole.Name = Pole.Name;
                pole.Manager_ID = Pole.Manager_ID;

                PoleRepo.Edit(pole);
            }
            return RedirectToAction("List");
        }

        public ActionResult Details(Guid id)
        {
            using(var dao = new NotesDeFraisEntities2())
            {
                PoleRepository PoleRepo = new PoleRepository(dao);

                PoleModel pole = new PoleModel(PoleRepo.GetById(id));

                return View("Details", pole);
            }
        }


    }
}