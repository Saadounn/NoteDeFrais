﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Projet_ELMOHCINE_EZZERRIFI.Startup))]
namespace Projet_ELMOHCINE_EZZERRIFI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
